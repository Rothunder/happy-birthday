import { AclService } from './../acl/acl.service';
import { Directive, EmbeddedViewRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import * as _ from 'lodash';

@Directive({ selector: '[hasAbility]' })
export class HasAbility {
    private _context: HasAbilityContext = new HasAbilityContext();
    private _thenTemplateRef: TemplateRef<HasAbilityContext> = null;
    private _elseTemplateRef: TemplateRef<HasAbilityContext> = null;
    private _thenViewRef: EmbeddedViewRef<HasAbilityContext> = null;
    private _elseViewRef: EmbeddedViewRef<HasAbilityContext> = null;

    constructor(
        private _viewContainer: ViewContainerRef,
        templateRef: TemplateRef<HasAbilityContext>,
        private aclService: AclService
    ) {
        this._thenTemplateRef = templateRef;
    }

    @Input()
    set hasAbility(ability: any) {
        let abilities = _.isArray(ability) ? ability : [ability];

        this._context.$implicit = this.aclService.canAny(abilities);

        this._updateView();
    }

    private _updateView() {
        if (this._context.$implicit) {
            if (!this._thenViewRef) {
                this._viewContainer.clear();
                this._elseViewRef = null;
                if (this._thenTemplateRef) {
                    this._thenViewRef =
                        this._viewContainer.createEmbeddedView(this._thenTemplateRef, this._context);
                }
            }
        } else {
            if (!this._elseViewRef) {
                this._viewContainer.clear();
                this._thenViewRef = null;
                if (this._elseTemplateRef) {
                    this._elseViewRef =
                        this._viewContainer.createEmbeddedView(this._elseTemplateRef, this._context);
                }
            }
        }
    }
}

export class HasAbilityContext { public $implicit: any = null; }
