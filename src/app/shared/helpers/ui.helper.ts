import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UiHelper {
  isLoading = false;
  loader: HTMLIonLoadingElement;
  mainLoader: any;

  constructor(
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) { }



  public async displayErrorAlert(displayMessage) {
    await this.alertCtrl.create({
      header: 'An error occured!',
      message: displayMessage,
      buttons: ['OK']
    }).then(alertEl => {
      alertEl.present();
    });
  }



  public async displayMessageAlert(title: string , displayMessage: string) {
    await this.alertCtrl.create({
      header: title,
      message: displayMessage,
      buttons: ['OK']
    }).then(alertEl => {
      alertEl.present();
    });
  }


  public async createConfirmAlert(title: string , displayMessage: string) {
     this.alertCtrl.create({
      header: title,
      message: displayMessage,
      buttons: [ {
        text: 'Cancel',
        role: 'cancel',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Confirm',
        handler: () => {
          return {confirm: true};
        }
      }]
    });
  }



  public displayToast(message, duration, position) {
    this.toastCtrl.create({
      message,
      duration,
      position,
      color: 'medium'
    }).then(toastEl => {
      toastEl.present();
    });
  }


  async showLoader(displayMessage) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      message: displayMessage,
      duration: 7000,
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().catch(() => { return; });
        }
      });
    });
  }


async hideLoader() {
  this.isLoading = false;
  return await this.loadingCtrl.dismiss().catch(() => { return; });
}



  // showLoader(displayMessage) {
  //   return  this.loadingCtrl.create({
  //     message: displayMessage
  //   }).then((loaderElement) => {
  //   this.isLoading = true;
  //   loaderElement.present();
  //   });
  // }

  // hideLoader() {

  //   return  this.loadingCtrl.dismiss(res => {
  //     this.isLoading = false;
  //   }).catch(() => {   this.isLoading = false;  return; });
  // }
}
