//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { AngularFirestore  } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})

export class FirebaseHelper {

  constructor(
    public fireStoreDB: AngularFirestore,
  ) { }

  public enrichFirebaseRes(firebaseResponse) {
    if (firebaseResponse.docs) {
      return firebaseResponse.docs.map(doc => {
        return {
          id: doc.id,
          ...doc.data()
        };
      });
    } else {
      return firebaseResponse.map(item => {
        return {
          ...item.payload.doc.data()
        };
      });
    }
  }

  public enrichFireResBirthdays(firebaseResponse) {
    if (firebaseResponse.docs) {
      return firebaseResponse.docs.map(doc => {
        const birthday = { id: doc.id, ...doc.data()  };
        const birthDate = (birthday.birthDate) ? birthday.birthDate.toDate() : null;
        return {
          ...birthday,
          birthDate
        };
      });
    } else {
      return firebaseResponse.map(item => {
        const birthday = { ...item.payload.doc.data()};
        const birthDate = (birthday.birthDate) ? birthday.birthDate.toDate() : null;
        return {
          ...birthday,
          birthDate
        };
      });
    }
  }

  public enrichDocument(document) {
    return {
      ...document.data()
    };
  }

  public generateFirebaeId () {
    return this.fireStoreDB.createId();
  }
}
