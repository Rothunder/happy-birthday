//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UtilityHelper {
  constructor( ) { }

  public genDifferencesObj(newObj, oldObj) {
    const changedObj = Object.assign({}, newObj);

    let changedData: any;
    let modifiedFields = [];

    //filter itms that do not have equal values and add them to changedchangedData
    Object.keys(oldObj)
      .filter(k => oldObj[k] !== newObj[k])
      .forEach(element => {
        if (newObj[element] !== null && newObj[element] !== undefined) {
          if (element === 'birthDate') {
            const oldDate = (oldObj[element] === null) ? oldObj[element] : oldObj[element].toISOString().slice(5, 10);
            const newDate = (newObj[element] === null) ? newObj[element] : newObj[element].toISOString().slice(5, 10);
            console.log("oldDate-", oldDate);
            console.log("newDate-", newDate);
            if (oldDate !== newDate) {
              modifiedFields.push(element);
              changedData = { ...changedData, [element]: changedObj[element] };
            }
          }
          else {
            modifiedFields.push(element);
            changedData = { ...changedData, [element]: changedObj[element] };
          }
        }
      });

    return { changedData, modifiedFields };
  }
}
