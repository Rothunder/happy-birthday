//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
//---------------DIRECTIVES----------------------------------------------------------------------------------------------------------------//
import { HasAbility } from './directives/has-ability.directive';
import { InputDisabledDirective } from './directives/disable.directive';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [InputDisabledDirective, HasAbility],
  exports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    InputDisabledDirective,
    HasAbility],
})
export class SharedModule { }
