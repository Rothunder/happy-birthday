
//----------------Birthday Event Types-----------------------
export const birthdayAddEvent = 'BIRTHDAY_ADD_EVENT';
export const birthdayUpdateEvent = 'BIRTHDAY_UPDATE_EVENT';
export const birthdayDeleteEvent = 'BIRTHDAY_DELETE_EVENT';

//------------Birthday Friendly Log Messages -----------------------
export const birthdayAddedDisplayLabel = 'Birthday Added';
export const birthdayUpdatedDisplayLabel = 'Birthday Updated';
export const birthdayDeletedDisplayLabel = 'Birthday Deleted';



