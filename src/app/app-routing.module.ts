//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
//---------------COMPONENTS/PAGES/SERVICES----------------------------------------------------------------------------------------------------------------//
import { AuthGuard } from './auth/auth.guard';
import { LoginPage } from './auth/pages/login/login.page';
import { BirthdayListPage } from './birthdays/pages/birthday-list/birthday-list.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'birthdays',
    pathMatch: 'full'
  },
  {
    path: 'birthdays',
    component: BirthdayListPage,
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    component: LoginPage
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
