//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { AuthDataStoreModule } from './store/auth-data-store.module';
//---------------COMPONENTS/PAGES/MODULES-------------------------------------------------------------------------------------------------------//
import { LoginPageModule } from './pages/login/login.module';
import { ForgotPageModule } from './pages/forgot-password/forgot-password.module';
import { SignUpPageModule } from './pages/signup/signup.module';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    AuthRoutingModule,
    AuthDataStoreModule,
    ForgotPageModule,
    SignUpPageModule,
    LoginPageModule,
  ],
})
export class AuthModule {}
