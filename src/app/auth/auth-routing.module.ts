import { SignUpPage } from './pages/signup/signup.page';
import { ForgotPasswordPage } from './pages/forgot-password/forgot-password.page';
import { LoginPage } from './pages/login/login.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: LoginPage,
  },
  {
    path: 'auth/signup',
    component: SignUpPage
  },
  {
    path: 'auth/forgot-password',
    component: ForgotPasswordPage
  },
  {
    path: '',
    redirectTo: '/auth',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
