//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import {SimpleCrypto} from 'simple-crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptHelper {
  encoder = new SimpleCrypto(environment.encodingKey);
  
  constructor() {}

  encode(text) {
    return this.encoder.encrypt(text);
  }

  decode(text) {
    return this.encoder.decrypt(text);
  }
}

