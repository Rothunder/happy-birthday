export interface AuthUserDbModel {
  uid?: string;
  displayName?: string;
  photoURL?: string;
  email?: string;
  emailVerified?: boolean;
  phoneNumber?: string;
  apiKey?: string;
  }

export const initialAuthUserDbModel = {
  uid: null,
  displayName: '',
  photoURL: null,
  email: null,
  emailVerified: false, 
  phoneNumber: null,
  apiKey: null,
  };


