import { UiHelper } from './../shared/helpers/ui.helper';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router,   CanActivate,  ActivatedRouteSnapshot,  RouterStateSnapshot, } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { AppState } from './../reducers/app.reducer';
import { selectIsAuthenticated } from './store/auth.selectors';
import { tap, take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {

  constructor(private store: Store<AppState>, private router: Router, public uiHelper: UiHelper) { }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {

    return this.store
      .pipe(
        select(selectIsAuthenticated),
        tap(loggedIn => {
          if (!loggedIn) {
            this.uiHelper.hideLoader();
            this.router.navigateByUrl('/auth');
          }
        }),
        take(1)
      );
  }



  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) { 
    return this.store
    .pipe(
      select(selectIsAuthenticated),
      tap(loggedIn => {
        if (!loggedIn) {
          this.uiHelper.hideLoader();
          this.router.navigateByUrl('/auth');
        }
      }),
      take(1)
    );
  }

}
