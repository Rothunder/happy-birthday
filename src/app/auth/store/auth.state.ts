import { AuthUserDbModel, initialAuthUserDbModel } from './../models/auth-user.model';

export interface AuthState {
    authDetails: AuthUserDbModel;
    authenticated: boolean;
}

export const AuthInitialState: AuthState = {
    authDetails: initialAuthUserDbModel,
    authenticated: false,
};
