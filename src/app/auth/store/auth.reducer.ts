import { AuthInitialState, AuthState } from './auth.state';
import { AuthActionTypes, AuthActions } from './auth.actions';


export function authReducer(state = AuthInitialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS: {
      return Object.assign({}, state, {
        authenticated: true,
        authDetails: {...action.payload.user},
      });
    }

    case AuthActionTypes.SET_UNAUTHENTICATED: {
      return Object.assign({}, state, {
        ...AuthInitialState
      });
    }

    case AuthActionTypes.LOGOUT_SUCCESS: {
      return Object.assign({}, state, {
        ...AuthInitialState
      });
    }

    case AuthActionTypes.ERROR_OCCURED: {
      return Object.assign({}, state, {
        error: action.payload.error,
      });
    }

    default:
      return state;
  }
}
