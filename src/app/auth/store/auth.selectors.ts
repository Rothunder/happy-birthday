import { createSelector } from '@ngrx/store';
import { AppState } from '../../reducers/app.reducer';

export const getAuthState = (state: AppState) => state.auth;

export const selectIsAuthenticated = createSelector(
  getAuthState,
  auth => auth.authenticated
);

export const selectCurrentUser = createSelector(
  getAuthState,
  auth => auth.authDetails
);

export const selectCreatedBy = createSelector(
  selectCurrentUser,
  user => {
    return {
      uid: user.uid,
      displayName: user.displayName,
      photoURL: user.photoURL,
      email: user.email,
      phoneNumber: user.phoneNumber,
    }
  }
);

