//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { map, tap, switchMap, catchError, take, } from 'rxjs/operators';
import { of, forkJoin, throwError } from 'rxjs';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//
import { AuthService } from './../services/auth.service';
import { UiHelper } from './../../shared/helpers/ui.helper';
import { AudioService } from './../../shared/services/audio-service';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as fromAuth from './auth.actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private uiHelper: UiHelper,
    private navCtrl: NavController,
    public audioService:AudioService,
  ) { }


  //------------------LOGIN -------------------------------------------
  @Effect()
  public login$ = this.actions$.pipe(
    ofType(fromAuth.AuthActionTypes.LOGIN),
    tap(() => {
      console.log('login$');
      this.uiHelper.showLoader('Loading...');
    }),
    switchMap((res: any) => {
      const credentials = res.payload;
      return this.authService.login(credentials).pipe(
        take(1),
        map(res => {
          return res.user.toJSON()
        }),
        switchMap((user: any) => {
          this.audioService.play('login');
          this.uiHelper.hideLoader();
          if (!user.emailVerified) {
            this.uiHelper.displayMessageAlert('Email Unverified', 'Check your email and verify your account.');
            return of(new fromAuth.ActionSuccess());
          } else {
            this.navCtrl.navigateRoot('/birthdays');
            return of(new fromAuth.LoginSuccess({ user: { ...user } }))
          }
        }),
        catchError(error => {
          console.log('error in login', error);
          this.uiHelper.displayErrorAlert(error);
          this.uiHelper.hideLoader();
          return of(new fromAuth.ErrorOccured({ error }));
        }),
      );
    }),
  );

  //----------------------SIGN UP------------------------------------------------
  @Effect()
  public signUp$ = this.actions$.pipe(
    ofType(fromAuth.AuthActionTypes.SIGN_UP),
    tap(() => {
      console.log('signUp$');
      this.uiHelper.showLoader('Loading...');
    }),
    switchMap((res: any) => {
      const userDetails = res.payload;
      this.audioService.play('save');
      //----------------------------------------------------------------------------------------
      return this.authService.signup(userDetails).pipe(
        map(signUpRes => ({ ...userDetails, id: signUpRes.user.uid })),
        switchMap((newUser) => {
          return forkJoin([
            this.authService.insertNewUserIntoDb(newUser).pipe(catchError(error => throwError(error))),
            this.authService.sendEmailVerification().pipe(catchError(error => throwError(error)))
          ]).pipe(
            switchMap(() => {
              this.uiHelper.hideLoader();
              this.navCtrl.navigateBack(['/auth']);
              this.uiHelper.displayMessageAlert('Account created!', 'Check your email for verifcation link');
              return of(new fromAuth.SignUpSuccess());
            }),
            catchError(error => {
              return throwError(error);
            }),
          );
        }),
        catchError(error => {
          console.log('error in signUp', error);
          this.uiHelper.hideLoader();
          this.uiHelper.displayErrorAlert(error);
          return of(new fromAuth.ErrorOccured({ error }));
        })
      );
    }),
  );

  //------------LOGOUT----------------------------------------------------
  @Effect()
  public logOut$ = this.actions$.pipe(
    ofType(fromAuth.AuthActionTypes.LOGOUT),
    tap(() => {
      console.log('logOut$');
      this.uiHelper.showLoader('Logging Out...');
    }),
    switchMap(() => {
      this.audioService.play('save');
      return this.authService.logout().pipe(
        switchMap(() => {
          return of(new fromAuth.LogoutSuccess());
        }),
        tap(() => {
          this.uiHelper.hideLoader();
          this.navCtrl.navigateRoot(['/auth']);
          this.uiHelper.displayToast('Logged out successfully', 1000, 'top');
        }),
        catchError(error => {
          console.log('error in get logOut', error);
          this.uiHelper.displayErrorAlert(error);
          this.uiHelper.hideLoader();
          return of(new fromAuth.ErrorOccured({ error }));
        }),
      );
    }
    ),
  );

  // ------------FORGOT PASSWORD----------------------------------------------------
  @Effect()
  public forgotPassword$ = this.actions$.pipe(
    ofType(fromAuth.AuthActionTypes.FORGOT_PASSWORD),
    tap(() => {
      console.log('forgotPassword$');
      this.uiHelper.showLoader('Loading...');
      this.audioService.play('save');
    }),
    switchMap((res: any) => {
      const email = res.payload.email;
      this.audioService.play('save');
      //------------------------------------------------------------------------------------------------------
      return this.authService.forgotPassword(email).pipe(
        switchMap(() => {
          this.uiHelper.hideLoader();
          this.navCtrl.back();
          this.uiHelper.displayToast('Password reset Sent!  Check your email', 5000, 'top');
          return of(new fromAuth.ActionSuccess());
        }),
        catchError(error => {
          console.log('error in get forgotPassword', error);
          this.uiHelper.hideLoader();
          this.uiHelper.displayErrorAlert(error);
          return of(new fromAuth.ErrorOccured({ error }));
        }),
      );
    }
    ),
  );
}
