import { Action } from '@ngrx/store';

export enum AuthActionTypes {
  LOGIN                       = '[Auth] Login',
  LOGIN_SUCCESS               = '[Auth] Login Success',

  SIGN_UP                     = '[Auth] Sign Up',
  SIGN_UP_SUCCESS             = '[Auth] Sign Up Success',

  FORGOT_PASSWORD             = '[Auth] Forgot Password',
  FORGOT_PASSWORD_SUCCESS     = '[Auth] Forgot Password Success',

  LOGOUT                      = '[Auth] Logout',
  LOGOUT_SUCCESS              = '[Auth] Logout Success',

  ERROR_OCCURED               = '[Auth] Error Occured',
  ACTION_SUCCESS              = '[Auth] Action was Successfull',

  SET_UNAUTHENTICATED         = '[Auth] Set Unathenticated',
}


//---------------------Login------------------------------//
export class Login implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: { email: string, password: string }) { }
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: { user: any }) { }
}


//---------------------Sign Up------------------------------//
export class SignUp implements Action {
  readonly type = AuthActionTypes.SIGN_UP;
  constructor(public payload: { user: any }) { }
}

export class SignUpSuccess implements Action {
  readonly type = AuthActionTypes.SIGN_UP_SUCCESS;
}


//---------------------Forgot Password----------------------//
export class ForgotPassword implements Action {
  readonly type = AuthActionTypes.FORGOT_PASSWORD;
  constructor(public payload: {email: string}) { }
}

export class ForgotPasswordSuccess implements Action {
  readonly type = AuthActionTypes.FORGOT_PASSWORD_SUCCESS;
}

//---------------------Log Out----------------------//
export class Logout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class LogoutSuccess implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS;
}

//----------------Set Unathenticated----------------------//
export class SetUnauthenticated implements Action {
  readonly type = AuthActionTypes.SET_UNAUTHENTICATED;
}


//------------Error  ------------------------------//
export class ErrorOccured implements Action {
  readonly type = AuthActionTypes.ERROR_OCCURED;
  constructor(public payload: { error: any}) {}
}

//------------Empty Action Success  ------------------------------//
export class ActionSuccess implements Action {
  readonly type = AuthActionTypes.ACTION_SUCCESS;
}


export type AuthActions =
  | Login
  | LoginSuccess

  | SignUp
  | SignUpSuccess

  | ForgotPassword
  | ForgotPasswordSuccess

  | Logout
  | LogoutSuccess

  | SetUnauthenticated

  | ErrorOccured
  | ActionSuccess;
