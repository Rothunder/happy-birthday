//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './../../../shared/shared.module';
//---------------COMPONENTS/PAGES----------------------------------------------------------------------------------------------------------------//
import { SignUpPage } from './signup.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpPage
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SignUpPage]
})
export class SignUpPageModule {}
