//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { regexValidators } from 'src/app/shared/helpers/validators.helper';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//---------------DATA_STORE----------------------------------------------------------------------------------------------------------------------//
import { AppState } from './../../../reducers/app.reducer';
import * as authActions from './../../store/auth.actions';
import { Store } from '@ngrx/store';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignUpPage implements OnInit {
  formObj: FormGroup;
  phoneMask: any;


  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.formObj = this.fb.group({
      email: ['', Validators.compose([Validators.pattern(regexValidators.email), Validators.required, Validators.minLength(1)])],
      name: ['', Validators.required],
      password: ['', Validators.compose([Validators.maxLength(20), Validators.minLength(3), Validators.required])],
      passwordConfirm: ['', Validators.compose([Validators.required])],
    }, { validator: this.checkPasswords });
  }

  onSubmit() {
    const formValue = this.formObj.value;
    if (this.formObj.valid) {
      this.store.dispatch(new authActions.SignUp({ ...formValue }));
    }
  }

  goToLogin() {
    this.navCtrl.navigateBack('/auth');
  }


  checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.passwordConfirm.value;

    return pass === confirmPass ? null : { notSame: true };
  }

}
