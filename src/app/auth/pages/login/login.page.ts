//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//--------------DATA_STORE----------------------------------------------------------------------------------------------------------------------//
import { AppState } from './../../../reducers/app.reducer';
import * as authActions from './../../store/auth.actions';
import { Storage } from '@ionic/storage';
import { Store } from '@ngrx/store';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//
import { regexValidators } from 'src/app/shared/helpers/validators.helper';
import { EncryptHelper } from './../../helpers/encrypt.helper';



@Component({
  selector: 'app-auth',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  formObj: FormGroup;

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private store: Store<AppState>,
    private storage: Storage,
    private encryptHelper: EncryptHelper,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.formObj = this.fb.group({
      email: ['', Validators.compose([Validators.pattern(regexValidators.email), Validators.required, Validators.minLength(1)])],
      password: ['', Validators.compose([Validators.maxLength(20), Validators.minLength(6), Validators.required])],
      rememberMe: ['', []],
    });
    this.checkPreviousRememberMe();
  }

  async onLogin() {
    const formValue = this.formObj.value;

    if (this.formObj.valid) {
     await this.handleRememberMe(formValue);
      this.store.dispatch(new authActions.Login({ email: formValue.email, password: formValue.password }));
    }
  }


  onGoToCreateAccount() {
    this.navCtrl.navigateForward(['/auth/signup']);
  }


  onGoToForgotPassword() {
    this.navCtrl.navigateForward(['/auth/forgot-password']);
  }

  //------------------------------------------------- ***UTILITY FUNCTIONS*** ---------------------------------------------------------------------//
  async handleRememberMe(formValue) {
    const encryptedPassword = await this.encryptHelper.encode(formValue.password);
    const authObject = await {
      email: formValue.email,
      password: encryptedPassword,
      rememberMe: formValue.rememberMe
    };
    if (authObject.rememberMe) {
      this.storage.set('rememberMeObj', authObject);
    } else {
      this.storage.set('rememberMeObj', null);
    }
  }

  async checkPreviousRememberMe() {
    const rememberMeObj = await this.storage.get('rememberMeObj');

    if (rememberMeObj) {
      if (rememberMeObj.password !== null && rememberMeObj.email !== null) {
        const decryptPassword = await this.encryptHelper.decode(rememberMeObj.password);
        const storageAuthObj = {
          email: rememberMeObj.email,
          password: decryptPassword,
          rememberMe: true
        };
        this.formObj.patchValue({
          ...storageAuthObj
        });
      } else {
        this.formObj.patchValue({
          rememberMe: false
        });
      }
    } else {
      this.formObj.patchValue({
        rememberMe: false
      });
    }
  }
}


