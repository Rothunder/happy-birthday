//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController  } from '@ionic/angular';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { AppState } from './../../../reducers/app.reducer';
import * as authActions from './../../store/auth.actions';
import { Store } from '@ngrx/store';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//
import { regexValidators } from '../../../shared/helpers/validators.helper';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})

export class ForgotPasswordPage implements OnInit {
  formObj: FormGroup;

  constructor(
              private fb: FormBuilder,
              private store: Store<AppState>,
              private navCtrl: NavController,
              ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.formObj = this.fb.group({
      email: ['', Validators.compose([Validators.pattern(regexValidators.email), Validators.required, Validators.minLength(1)])],
    });
  }

  onForgotPassword() {
    const email = this.formObj.value;

    if (this.formObj.valid) {
      this.store.dispatch(new authActions.ForgotPassword(email));
    }
  }

  goToLogin() {
    this.navCtrl.navigateBack('/auth');
   }
}
