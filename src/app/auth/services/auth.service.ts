//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';
import { Router } from '@angular/router';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { Store } from '@ngrx/store';
import { AppState } from './../../reducers/app.reducer';
import * as fromAuth from '../store/auth.actions';
//---------------FIREBASE------------------------------------------------------------------------------------------------------------------------//
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { initialAuthUserDbModel } from '../models/auth-user.model';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public http: HttpClient,
    public fireStoreAuth: AngularFireAuth,
    private fireStoreDB: AngularFirestore,
    private router: Router,
    private store: Store<AppState>,
  ) { }


  initAuthListener() {
    this.fireStoreAuth.authState.subscribe(fireUser => {
      if (!fireUser) {
        this.router.navigate(['/auth']);
        this.store.dispatch(new fromAuth.SetUnauthenticated());
      }
    });
  }

  createUserWithEmailAndPassword(email: string, password: string) {
    return from(this.fireStoreAuth.auth.createUserWithEmailAndPassword(email, password));
  }

  signup(userDetails) {
    return from(this.fireStoreAuth.auth.createUserWithEmailAndPassword(userDetails.email, userDetails.password));
  }


  insertNewUserIntoDb(newUser) {
    return from(this.fireStoreDB.collection('users').doc(newUser.id)
      .set({
        ...initialAuthUserDbModel,
        id: newUser.id,
        name: newUser.name,
        email: newUser.email,
        created: new Date()
      }));
  }

  forgotPassword(email) {
    return from(firebase.auth().sendPasswordResetEmail(email));
  }

  sendEmailVerification() {
    return from(this.fireStoreAuth.auth.currentUser.sendEmailVerification());
  }


  login(formObj) {
    return from(this.fireStoreAuth.auth.signInWithEmailAndPassword(formObj.email, formObj.password));
  }

  logout() {
    return from(this.fireStoreAuth.auth.signOut());
  }
}