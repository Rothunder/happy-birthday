//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Component } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { AppState } from './reducers/app.reducer';
import { Store } from '@ngrx/store';
import * as fromAuth from './auth/store/auth.actions';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//
import { AuthService } from './auth/services/auth.service';
import { AudioService } from './shared/services/audio-service';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  selectedPath = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authService: AuthService,
    public audioService: AudioService,
    private store: Store<AppState>,

  ) {
    this.initializeApp();

    this.router.events.pipe(distinctUntilChanged()).subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
    this.authService.initAuthListener();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.audioService.preload('save', 'assets/audio/save2.mp3');
      this.audioService.preload('login', 'assets/audio/login2.wav');
    });
  }
  
  onLogOut() {
    this.store.dispatch(new fromAuth.Logout());
  }
}
