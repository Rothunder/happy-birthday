export interface BirthdayDBModel {
  id?: string;
  name?: string;
  birthDate?: any;
  email?: string;
  message?: string;
}


export const  initialBirthday = {
  id: null,
  name: '',
  birthDate: null,
  email: null,
  message: ''
};

