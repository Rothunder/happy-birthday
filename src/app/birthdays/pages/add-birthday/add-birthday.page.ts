//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonSlides, NavController, AlertController } from '@ionic/angular';
//---------------DATA_STORE----------------------------------------------------------------------------------------------------------------------//
import { Store } from '@ngrx/store';
import { AppState } from './../../../reducers/app.reducer';
import * as fromBirthdays from './../../store/birthdays.actions';
//---------------Services/Helpers----------------------------------------------------------------------------------------------------------------//
import { regexValidators } from 'src/app/shared/helpers/validators.helper';
import { FirebaseHelper } from './../../../shared/helpers/firebase.helper';


@Component({
  selector: 'app-add-birthday',
  templateUrl: './add-birthday.page.html',
  styleUrls: ['./add-birthday.page.scss'],
})
export class AddBirthdayPage implements OnInit {
  @ViewChild('sliderContainer', { static: true }) slides: IonSlides;

  formObj: FormGroup;

  constructor(
    private fb: FormBuilder,
    private alertCtrl: AlertController,
    private store: Store<AppState>,
    private firebaseHelper: FirebaseHelper,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.formObj = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', Validators.compose([Validators.pattern(regexValidators.email), Validators.required])],
      birthDate: ['', [Validators.required]],
      message: ['', [Validators.required]],
    });
    this.slides.lockSwipes(true);
  }

  async nextSlide() {
    await this.slides.lockSwipes(false);
    await this.slides.slideNext();
    await this.slides.lockSwipeToPrev(false);
    await this.slides.lockSwipeToNext(true);
  }


  onCancel(){
    this.store.dispatch(new fromBirthdays.ClearBirthdaysToBeAddedSuccess());
    this.navCtrl.back();
  }

  onSubmit(){
    const birthday = {
      ...this.formObj.value,
      id: this.firebaseHelper.generateFirebaeId(),
      birthDate: new Date(this.formObj.value.birthDate)
    };

    this.alertCtrl
      .create({
        header: 'Save Birthday Entry',
        message: 'Do want to add another birthday for someone else?',
        buttons: [
          {
            text: 'Save',
            handler: () => {
              this.store.dispatch(new fromBirthdays.CreateBirthdays({ birthday: { ...birthday, }}));
            }
          },
          {
            text: 'Add Another Birthday',
            handler: () => {
              this.store.dispatch(new fromBirthdays.AddAnotherBirthdaySuccess({ birthday: { ...birthday }}));
              this.formObj.reset();
              this.slides.slideTo(0);
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }


}
