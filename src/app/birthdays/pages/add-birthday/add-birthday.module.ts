//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './../../../shared/shared.module';
//---------------COMPONENTS/PAGES----------------------------------------------------------------------------------------------------------------//
import { AddBirthdayPage } from './add-birthday.page';

const routes: Routes = [
  {
    path: '',
    component: AddBirthdayPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddBirthdayPage]
})
export class AddBirthdayPageModule {}
