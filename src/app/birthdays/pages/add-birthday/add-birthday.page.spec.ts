import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddBirthdayPage } from './add-birthday.page';

describe('AddBirthdayPage', () => {
  let component: AddBirthdayPage;
  let fixture: ComponentFixture<AddBirthdayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBirthdayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBirthdayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
