//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './../../../shared/shared.module';
//---------------COMPONENTS/PAGES----------------------------------------------------------------------------------------------------------------//
import { BirthdayListPage } from './birthday-list.page';
import { FilterBirthdays } from './../../pipes/birthdays-filter';

const routes: Routes = [
  {
    path: '',
    component: BirthdayListPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BirthdayListPage, FilterBirthdays]
})
export class BirthdayListPageModule {}
