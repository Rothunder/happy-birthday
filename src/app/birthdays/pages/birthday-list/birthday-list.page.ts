//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { Store } from '@ngrx/store';
import * as fromBirthday from './../../store/birthdays.actions';
import { AppState } from './../../../reducers/app.reducer';
import { selectBirthdayList } from './../../store/birthdays.selectors';
//---------------COMPONENTS/PAGES/MODELS----------------------------------------------------------------------------------------------------------------//
import { BirthdayDBModel } from './../../../birthdays/models/birthdays.model';

@Component({
  selector: 'app-birthday-list',
  templateUrl: './birthday-list.page.html',
  styleUrls: ['./birthday-list.page.scss'],
})
export class BirthdayListPage implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  birthdays: BirthdayDBModel [];
  searchQuery = '';

  constructor(
    private store: Store<AppState>, 
  ) { }

  ngOnInit() {
    this.store.select(selectBirthdayList).pipe(takeUntil(this.destroy$)).subscribe(res => {
      this.birthdays = res;
    })

    this.store.dispatch(new fromBirthday.GetBirthdayList());
  }


  onViewDetail(birthday) {
    this.store.dispatch(new fromBirthday.SetBirthdayDetail({ birthdayDetail: {...birthday}}));
  }

  onDelete(birthday) {
    this.store.dispatch(new fromBirthday.DeleteBirthday({ birthdayDetail: { ...birthday, } }));
  }

  //------------------------------------------------- ON DESTROY/LEAVE PAGE UNSUBSCRIBE -----------------------------------------------------------//
  ngOnDestroy() {
    console.log('Destroyed Birthday List Page');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
