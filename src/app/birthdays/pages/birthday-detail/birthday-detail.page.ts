//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
//---------------DATA_STORE----------------------------------------------------------------------------------------------------------------------//
import { Store } from '@ngrx/store';
import { AppState } from './../../../reducers/app.reducer';
import * as fromBirthdays from './../../store/birthdays.actions';
import { selectBirthdayDetail } from './../../store/birthdays.selectors';
//---------------Services/Helpers----------------------------------------------------------------------------------------------------------------//
import { regexValidators } from 'src/app/shared/helpers/validators.helper';
import { BirthdayDBModel } from './../../../birthdays/models/birthdays.model';

@Component({
  selector: 'app-birthday-detail',
  templateUrl: './birthday-detail.page.html',
  styleUrls: ['./birthday-detail.page.scss'],
})
export class BirthdayDetailPage implements OnInit, OnDestroy  {
  private destroy$: Subject<boolean> = new Subject<boolean>();
  formObj: FormGroup;
  birthdayDetail: BirthdayDBModel;

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    this.store.select(selectBirthdayDetail).pipe(takeUntil(this.destroy$)).subscribe(res => {
      this.birthdayDetail = res;
    });
    this.initForm();
  }

  initForm() {
    this.formObj = this.fb.group({
      name: [this.birthdayDetail.name, [Validators.required]],
      email: [this.birthdayDetail.email, Validators.compose([Validators.pattern(regexValidators.email), Validators.required])],
      birthDate: [this.birthdayDetail.birthDate, [Validators.required]],
      message: [this.birthdayDetail.message, [Validators.required]],
    });
  }

  onSubmit() {
    const birthday = {...this.formObj.value, birthDate: new Date(this.formObj.value.birthDate)};
    this.store.dispatch(new fromBirthdays.UpdateBirthday({ birthdayDetail: { ...birthday}}));
  }
  
  //------------------------------------------------- ON DESTROY/LEAVE PAGE UNSUBSCRIBE -----------------------------------------------------------//
  ngOnDestroy() {
    console.log('Destroyed Birthday Detail Page');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
