//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './../../../shared/shared.module';
//---------------COMPONENTS/PAGES----------------------------------------------------------------------------------------------------------------//
import { BirthdayDetailPage } from './birthday-detail.page';

const routes: Routes = [
  {
    path: '',
    component: BirthdayDetailPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BirthdayDetailPage]
})
export class BirthdayDetailPageModule {}
