import { createSelector } from '@ngrx/store';
import { AppState } from '../../reducers/app.reducer';
import * as fromBirthdays from './birthdays.reducer';

export const getBirthdaysListState = (state: AppState) => state.birthdays.birthdayList;
export const getBirthdayDetailState = (state: AppState) => state.birthdays.birthdayDetail;
export const getBirthdaysToBeAdded = (state: AppState) => state.birthdays.birthdaysToBeAdded;;


export const selectBirthdayList = createSelector(
  getBirthdaysListState,
  fromBirthdays.selectBirthdays
);

export const selectBirthdaysToBeAdded = createSelector(
  getBirthdaysToBeAdded,
  fromBirthdays.selectBirthdays
);

export const selectBirthdayDetail = createSelector(
  getBirthdayDetailState,
  birthdayDetail => {
    const birthDate = (birthdayDetail.birthDate !== null) ? birthdayDetail.birthDate.toISOString() : null;
    return {
      ...birthdayDetail,
      birthDate
    }
  }
);


export const selectBirthdayCompareObj = createSelector(
  getBirthdayDetailState,
  birthday =>  {
    return {
      originalObj: { ...birthday},
       compareObj: {
        name: birthday.name,
         birthDate: birthday.birthDate,
        email: birthday.email,
         message: birthday.message,
       }
    }
  }
);
