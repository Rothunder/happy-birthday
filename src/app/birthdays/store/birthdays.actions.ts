import { BirthdayDBModel } from './../../birthdays/models/birthdays.model';
import { Action } from '@ngrx/store';
 
export enum BirthdaysActionTypes {
  GET_BIRTHDAY_LIST                       = '[Birthdays] Get Birthday List',
  GET_BIRTHDAY_LIST_SUCCESS               = '[Birthdays] Get Birthday List Success',

  CREATE_BIRTHDAYS                        = '[Birthdays] Create Birthdays',
  CREATE_BIRTHDAYS_SUCCESS                = '[Birthdays] Create Birthdays Success',

  UPDATE_BIRTHDAY                         = '[Birthdays] Update Birthday',
  UPDATE_BIRTHDAY_SUCCESS                 = '[Birthdays] Update Birthday Success',

  DELETE_BIRTHDAY                         = '[Birthdays] Delete Birthday',
  DELETE_BIRTHDAY_SUCCESS                 = '[Birthdays] Delete Birthday Success',

  ADD_ANOTHER_BIRTHDAY_SUCCESS            = '[Birthdays] Add Another Birthday Success',

  SET_BIRTHDAY_DETAIL_SUCCESS             = '[Birthdays] Set Birthday Detail Success',

  CLEAR_BIRTHDAYS_TO_BE_ADDED_SUCCESS     = '[Birthdays] Clear Birthdays to be added Success',

  NO_CHANGES_DETECTED                     = '[Birthdays] No Changes Detected',

  ERROR_OCCURED                           = '[Birthdays] Error Occured',
  ACTION_SUCCESS                          = '[Birthdays] Action was Successfull',
}

//------------------------- GET BIRTHDAY LIST -----------------------------------//
export class GetBirthdayList implements Action {
  readonly type = BirthdaysActionTypes.GET_BIRTHDAY_LIST;
}

export class GetBirthdayListSuccess implements Action {
  readonly type = BirthdaysActionTypes.GET_BIRTHDAY_LIST_SUCCESS;
  constructor(public payload: { birthdayList: BirthdayDBModel[] }  ) {}
}

//------------------------- CREATE BIRTHDAYS------------------------------------------//
export class CreateBirthdays implements Action {
  readonly type = BirthdaysActionTypes.CREATE_BIRTHDAYS;
  constructor(public payload: { birthday: BirthdayDBModel }) { }
}

export class CreateBirthdaysSuccess implements Action {
  readonly type = BirthdaysActionTypes.CREATE_BIRTHDAYS_SUCCESS;
  constructor(public payload: { birthdayList: BirthdayDBModel[] }) { }
}

//------------------------- ADD ANOTHER BIRTHDAY------------------------------------------//
export class AddAnotherBirthdaySuccess implements Action {
  readonly type = BirthdaysActionTypes.ADD_ANOTHER_BIRTHDAY_SUCCESS;
  constructor(public payload: { birthday: BirthdayDBModel } ) {}
}


//------------------------- UPDATE BIRTHDAY--- ---------------------------------------//
export class UpdateBirthday implements Action {
  readonly type = BirthdaysActionTypes.UPDATE_BIRTHDAY;
  constructor(public payload: { birthdayDetail: BirthdayDBModel } ) {}
}


export class UpdateBirthdaySuccess implements Action {
  readonly type = BirthdaysActionTypes.UPDATE_BIRTHDAY_SUCCESS;
  constructor(public payload: { birthdayDetail: BirthdayDBModel } ) {}
}

//------------------------- DELETE BIRTHDAY--- ---------------------------------------//
export class DeleteBirthday implements Action {
  readonly type = BirthdaysActionTypes.DELETE_BIRTHDAY;
  constructor(public payload: { birthdayDetail: BirthdayDBModel }) { }
}


export class DeleteBirthdaySuccess implements Action {
  readonly type = BirthdaysActionTypes.DELETE_BIRTHDAY_SUCCESS;
  constructor(public payload: { birthdayDetail: BirthdayDBModel }) { }
}

//------------------------- SET BIRTHDAY DETAIL--- ---------------------------------------//
export class SetBirthdayDetail implements Action {
  readonly type = BirthdaysActionTypes.SET_BIRTHDAY_DETAIL_SUCCESS;
  constructor(public payload: { birthdayDetail: BirthdayDBModel }) { }
}

//------------------------- CLEAR BIRTHDAYS TO BE ADDED---------------------------------//
export class ClearBirthdaysToBeAddedSuccess implements Action {
  readonly type = BirthdaysActionTypes.CLEAR_BIRTHDAYS_TO_BE_ADDED_SUCCESS;
}

//------------------------- NO CHANGES DETECTED--- -----------------------------------//
export class NoChangesDetected implements Action {
  readonly type = BirthdaysActionTypes.NO_CHANGES_DETECTED;
}


//------------------------- ERROR OCCURED  -------------------------------------//
export class ErrorOccured implements Action {
  readonly type = BirthdaysActionTypes.ERROR_OCCURED;

  constructor(public payload: { error: any}) {}
}

//------------------------- ACTION SUCCESS  -----------------------------------//
export class ActionSuccess implements Action {
  readonly type = BirthdaysActionTypes.ACTION_SUCCESS;
}



export type BirthdaysActions =
| GetBirthdayList
| GetBirthdayListSuccess

| CreateBirthdays
| CreateBirthdaysSuccess

| AddAnotherBirthdaySuccess

| UpdateBirthday
| UpdateBirthdaySuccess

| DeleteBirthday
| DeleteBirthdaySuccess

| SetBirthdayDetail

| ClearBirthdaysToBeAddedSuccess

| NoChangesDetected
| ErrorOccured
| ActionSuccess;
