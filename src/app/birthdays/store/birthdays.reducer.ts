import { BirthdaysActionTypes, BirthdaysActions } from './birthdays.actions';
import { BirthdaysInitialState, BirthdaysState, birthdaysAdapter } from './birthdays.state';


export function birthdaysReducer(state = BirthdaysInitialState, action: BirthdaysActions): BirthdaysState {
  switch (action.type) {

    case BirthdaysActionTypes.GET_BIRTHDAY_LIST_SUCCESS: {
      return Object.assign({}, state, {
        birthdayList: birthdaysAdapter.addAll(action.payload.birthdayList, state.birthdayList),
      });
    }

    case BirthdaysActionTypes.CREATE_BIRTHDAYS_SUCCESS: {
      return Object.assign({}, state, {
        birthdayList: birthdaysAdapter.addMany(action.payload.birthdayList, state.birthdayList),
        birthdaysToBeAdded: birthdaysAdapter.removeAll(state.birthdaysToBeAdded),
      });
    }

    case BirthdaysActionTypes.ADD_ANOTHER_BIRTHDAY_SUCCESS: {
      return Object.assign({}, state, {
        birthdaysToBeAdded: birthdaysAdapter.addOne(action.payload.birthday, state.birthdaysToBeAdded),
      });
    }

    case BirthdaysActionTypes.UPDATE_BIRTHDAY_SUCCESS: {
      return Object.assign({}, state, {
        birthdayList: birthdaysAdapter.updateOne({ id: action.payload.birthdayDetail.id, changes: action.payload.birthdayDetail }, state.birthdayList),
        birthdayDetail: action.payload.birthdayDetail
      });
    }

    case BirthdaysActionTypes.DELETE_BIRTHDAY_SUCCESS: {
      return Object.assign({}, state, {
        birthdayList: birthdaysAdapter.removeOne(action.payload.birthdayDetail.id, state.birthdayList),
      });
    }

    case BirthdaysActionTypes.SET_BIRTHDAY_DETAIL_SUCCESS: {
      return Object.assign({}, state, {
        birthdayDetail: { ...state.birthdayDetail, ...action.payload.birthdayDetail },
      });
    }

    case BirthdaysActionTypes.CLEAR_BIRTHDAYS_TO_BE_ADDED_SUCCESS: {
      return Object.assign({}, state, {
        birthdaysToBeAdded: birthdaysAdapter.removeAll(state.birthdaysToBeAdded)
      });
    }

    default:
      return state;
  }
}

export const { selectAll: selectBirthdays } = birthdaysAdapter.getSelectors();
