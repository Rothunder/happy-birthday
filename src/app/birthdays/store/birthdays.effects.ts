//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import {tap, switchMap, catchError, withLatestFrom, take } from 'rxjs/operators';
import { of } from 'rxjs';
//---------------SERVICES/HELPERS----------------------------------------------------------------------------------------------------------------//
import { UiHelper } from './../../shared/helpers/ui.helper';
import { BirthdaysService } from './../services/birthdays.service';
import { AudioService } from './../../shared/services/audio-service';
import { FirebaseHelper } from './../../shared/helpers/firebase.helper';
import { UtilityHelper } from './../../shared/helpers/utility-helper';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AppState } from './../../reducers/app.reducer';
import { Store } from '@ngrx/store';
import * as fromBirthdays from './birthdays.actions';
import { selectCreatedBy } from './../../auth/store/auth.selectors';
import { selectBirthdaysToBeAdded } from './birthdays.selectors';
import { selectBirthdayCompareObj } from './birthdays.selectors';


@Injectable()
export class BirthdaysEffects {
  constructor(
    private actions$: Actions,
    private birthdayService: BirthdaysService,
    private uiHelper: UiHelper,
    private firebaseHelper: FirebaseHelper,
    private navCtrl: NavController,
    private utilHelper: UtilityHelper,
    private store: Store<AppState>,
    private audioService: AudioService,
  ) { }

  //------------------GET BIRTHDAY LIST --------------------------------------------------------------------
  @Effect()
  public getBirthdayList$ = this.actions$.pipe(
    ofType(fromBirthdays.BirthdaysActionTypes.GET_BIRTHDAY_LIST),
    tap(() => {
      console.log('getBirthdayList$'),
      this.uiHelper.showLoader('Loading...');
    }),
    withLatestFrom(this.store.select(selectCreatedBy)),
    switchMap((res: any) => {
      const currentUserId = res[1].uid;
      //---------------------------------------------------------------------------------------------------
      return this.birthdayService.getBirthdayList(currentUserId).pipe(
        take(1),
        switchMap((birthdayList: any) => {
          const enrichedBirthdayList = this.firebaseHelper.enrichFireResBirthdays(birthdayList);
          this.uiHelper.hideLoader();
          return of(new fromBirthdays.GetBirthdayListSuccess({ birthdayList: enrichedBirthdayList }));
        }),
        catchError(error => {
          console.log('getBirthdayList$ error?', error);
          this.uiHelper.hideLoader();
          this.uiHelper.displayErrorAlert(error.message);
          return of(new fromBirthdays.ErrorOccured(error));
        })
      );
    }),
  );


  //------------------Create Birthday--------------------------------------------------------------------
  @Effect()
  public createBirthdays$ = this.actions$.pipe(
    ofType(fromBirthdays.BirthdaysActionTypes.CREATE_BIRTHDAYS),
    tap(() => {
      console.log('createBirthdays$'),
      this.uiHelper.showLoader('Loading...');
    }),
    withLatestFrom(this.store.select(selectBirthdaysToBeAdded), this.store.select(selectCreatedBy)),
    switchMap((res: any) => {
      const birthday = {...res[0].payload.birthday};
      const birthdays = [...res[1], { ...birthday}]
      const currentUser = res[2];

      const updatedBirthdays = birthdays.map(birthday => {
        return {
          ...birthday,
          createdBy: {...currentUser},
          created: new Date ()
        }
      })
      this.audioService.play('login');
      //---------------------------------------------------------------------------------------------------
      return this.birthdayService.createBirthdays(updatedBirthdays, currentUser).pipe(
        switchMap(() => {
          this.uiHelper.hideLoader();
          this.uiHelper.displayToast('Birthdays were added', 1000, 'top');
          this.navCtrl.navigateBack(['/birthdays']);
          return of(new fromBirthdays.CreateBirthdaysSuccess({ birthdayList: updatedBirthdays }));
        }),
        catchError(error => {
          console.log('addBirthdays$ error?', error);
          this.navCtrl.back();
          this.uiHelper.hideLoader();
          this.uiHelper.displayErrorAlert(error.message);
          return of(new fromBirthdays.ErrorOccured(error));
        })
      );
    }),
  );


  //------------------Delete Birthday--------------------------------------------------------------------
  @Effect()
  public deleteBirthday$ = this.actions$.pipe(
    ofType(fromBirthdays.BirthdaysActionTypes.DELETE_BIRTHDAY),
    tap(() => {
      console.log('deleteBirthday$'),
        this.uiHelper.showLoader('Loading...');
    }),
    withLatestFrom(this.store.select(selectCreatedBy)),
    switchMap((res: any) => {
      const birthday = { ...res[0].payload.birthdayDetail };
      const currentUser = res[1];
      //---------------------------------------------------------------------------------------------------
      return this.birthdayService.deleteBirthday(birthday, currentUser).pipe(
        switchMap(() => {
          this.audioService.play('save');
          this.uiHelper.hideLoader();
          this.uiHelper.displayToast('Birthday was Deleted', 1000, 'top');
          return of(new fromBirthdays.DeleteBirthdaySuccess({ birthdayDetail: {...birthday} }));
        }),
        catchError(error => {
          console.log('deleteBirthday$ error?', error);
          this.uiHelper.hideLoader();
          this.uiHelper.displayErrorAlert(error.message);
          return of(new fromBirthdays.ErrorOccured(error));
        })
      );
    }),
  );

  //------------------Update Birthday --------------------------------------------------------------------
  @Effect()
  public updateBirthday$ = this.actions$.pipe(
    ofType(fromBirthdays.BirthdaysActionTypes.UPDATE_BIRTHDAY),
    tap(() => {
      this.uiHelper.showLoader('Loading...');
      console.log('updateBirthday$');
    }),
    withLatestFrom(this.store.select(selectBirthdayCompareObj),this.store.select(selectCreatedBy)),
    switchMap((res: any) => {
      const birthday = {...res[0].payload.birthdayDetail};
      const originalObj = res[1].originalObj;
      const compareObj = res[1].compareObj
      const diffObj = this.utilHelper.genDifferencesObj(birthday, compareObj);
      const currentUser = res[2];

      const modifiedFields = diffObj.modifiedFields;

      const updatedBirthday = {
        ...originalObj,
        ...birthday,
      };
      this.audioService.play('save');

      if (modifiedFields.length === 0) {
        return of(new fromBirthdays.NoChangesDetected());
      } else {
        return this.birthdayService.updateBirthday(updatedBirthday, originalObj, modifiedFields, currentUser ).pipe(
          switchMap(() => {
            this.uiHelper.hideLoader();
            this.uiHelper.displayToast('Birthday was Updated', 1000, 'top');
            return of(new fromBirthdays.UpdateBirthdaySuccess({ birthdayDetail: { ...updatedBirthday}}));
          }),
          catchError(errorDisplay => {
            console.log('error in updateLocation$', errorDisplay);
            this.uiHelper.hideLoader();
            this.uiHelper.displayErrorAlert(errorDisplay);
            return of(new fromBirthdays.ErrorOccured(errorDisplay));
          })
        );
      }
    }),
  );

  //------------------NO CHANGES DETECTED  --------------------------------------------------------------------
  @Effect()
  public noChangesDetected$ = this.actions$.pipe(
    ofType(fromBirthdays.BirthdaysActionTypes.NO_CHANGES_DETECTED),
    tap(() => {
      this.uiHelper.hideLoader();
      console.log('noChangesDetected$');
      this.uiHelper.displayToast('No Changes Detected', 1000, 'bottom');
    }),
    switchMap(() => {
      return of(new fromBirthdays.ActionSuccess());
    }),
  );
}
