import { BirthdayDBModel, initialBirthday } from './../../birthdays/models/birthdays.model';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

//-------Birthdays Entity Set Up------------------------------------------------
interface BirthdayEntity extends EntityState<BirthdayDBModel> {}
const adapterBirthday = createEntityAdapter<BirthdayDBModel>();
const birthdayEntityInitialState: BirthdayEntity = adapterBirthday.getInitialState({});
export const birthdaysAdapter = adapterBirthday;
//-----------------------------------------------------------------------------

export interface BirthdaysState {
    birthdayList: BirthdayEntity
    birthdayDetail: BirthdayDBModel
    birthdaysToBeAdded: BirthdayEntity
    error: any;
}

export const BirthdaysInitialState: BirthdaysState = {
    birthdayList: birthdayEntityInitialState,
    birthdayDetail: initialBirthday,
    birthdaysToBeAdded: birthdayEntityInitialState,
    error: null,
};
