//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BirthdaysEffects } from './birthdays.effects';
import * as fromBirthdays from './birthdays.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('birthdays', fromBirthdays.birthdaysReducer),
    EffectsModule.forFeature([BirthdaysEffects])
  ],
})
export class BirthdaysDataStoreModule {}
