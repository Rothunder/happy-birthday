//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
//---------------FIREBASE------------------------------------------------------------------------------------------------------------------------//
import { AngularFirestore } from '@angular/fire/firestore';
//---------------MODELS----------------------------------------------------------------------------------------------------------------//
import { BirthdayDBModel } from './../../birthdays/models/birthdays.model';
import { AuthUserDbModel } from './../../auth/models/auth-user.model';
import * as fromLogs from './../../shared/models/logs.model';

@Injectable({
  providedIn: 'root'
})
export class BirthdaysService {
  constructor(
    public fireStoreDB: AngularFirestore,
  ) { }

  getBirthdayList(userId) {
    return this.fireStoreDB.collection('birthdays', ref => ref.where('createdBy.uid', '==', userId)).get();
  }

  createBirthdays(birthdays: BirthdayDBModel[], currentUser: AuthUserDbModel) {
    const batch = this.fireStoreDB.firestore.batch();

    birthdays.forEach(birthday => {
      const birthdayLogId = this.fireStoreDB.createId();
      const birthdayLogRef = this.fireStoreDB.collection('logs').doc(birthdayLogId).ref;
      const birthdayRef = this.fireStoreDB.collection('birthdays').doc(birthday.id).ref;

      //-----------Birthday Log Obj----------------------------------------------
      const birthdayLogObj = {
        id: birthdayLogId,
        timeStamp: new Date(),
        event: fromLogs.birthdayAddEvent,
        lastUpdatedBy: currentUser,
        lastUpdated: new Date(),
        obj: { ...birthday },
        display: fromLogs.birthdayAddedDisplayLabel
      };

      batch.set(birthdayLogRef, birthdayLogObj);
      batch.set(birthdayRef, { ...birthday });
    });

    return from(batch.commit());
  }

  updateBirthday(birthday: BirthdayDBModel, originalObj: BirthdayDBModel, modifiedFields: any, currentUser: AuthUserDbModel) {
    const batch = this.fireStoreDB.firestore.batch();

    const updatedBirthdayLogId = this.fireStoreDB.createId();
    const updateBirthdayLogRef = this.fireStoreDB.collection('logs').doc(updatedBirthdayLogId).ref;
    const birthdayRef = this.fireStoreDB.collection('birthdays').doc(birthday.id).ref;

    //-----------Birthday Log Obj----------------------------------------------
    const birthdayLogObj = {
      id: updatedBirthdayLogId,
      timeStamp: new Date(),
      event: fromLogs.birthdayUpdateEvent,
      lastUpdatedBy: currentUser,
      lastUpdated: new Date(),
      originalObj,
      modifiedFields,
      obj: { ...birthday },
      display: fromLogs.birthdayUpdatedDisplayLabel
    };
    batch.set(updateBirthdayLogRef, birthdayLogObj);
    batch.update(birthdayRef, { ...birthday });

    return from(batch.commit());
  }

  deleteBirthday(birthday: BirthdayDBModel, currentUser: AuthUserDbModel) {
    const batch = this.fireStoreDB.firestore.batch();

    const deletedBirthdayLogId = this.fireStoreDB.createId();
    const deletedBirthdayLogRef = this.fireStoreDB.collection('logs').doc(deletedBirthdayLogId).ref;
    const deletedBirthdayRef = this.fireStoreDB.collection('birthdays').doc(birthday.id).ref;

    //-----------Birthday Log Obj----------------------------------------------
    const birthdayLogObj = {
      id: deletedBirthdayLogId,
      timeStamp: new Date(),
      event: fromLogs.birthdayDeleteEvent,
      lastUpdatedBy: currentUser,
      lastUpdated: new Date(),
      obj: { ...birthday },
      display: fromLogs.birthdayDeletedDisplayLabel
    };
    batch.set(deletedBirthdayLogRef, birthdayLogObj);
    batch.delete(deletedBirthdayRef);

    return from(batch.commit());
  }
}
