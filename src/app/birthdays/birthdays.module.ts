//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { BirthdaysRoutingModule } from './birthdays-routing.module';
//---------------DATA STORE----------------------------------------------------------------------------------------------------------------------//
import { BirthdaysDataStoreModule } from './store/birthdays-data-store.module';
//---------------COMPONENTS/PAGES/MODULES---------------------------------------------------------------------------------------------------//
import { AddBirthdayPageModule } from './pages/add-birthday/add-birthday.module';
import { BirthdayDetailPageModule } from './pages/birthday-detail/birthday-detail.module';
import { BirthdayListPageModule } from './pages/birthday-list/birthday-list.module';

@NgModule({
  imports: [
    SharedModule,
    BirthdaysDataStoreModule,
    AddBirthdayPageModule,
    BirthdayDetailPageModule,
    BirthdayListPageModule,
    BirthdaysRoutingModule,
  ],
})
export class BirthdaysModule {}
