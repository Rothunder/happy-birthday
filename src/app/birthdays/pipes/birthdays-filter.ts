import { BirthdayDBModel } from './../../birthdays/models/birthdays.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterBirthdays',
})
export class FilterBirthdays implements PipeTransform {
  transform(birthdays: BirthdayDBModel[], filter) {
    if (filter && filter.trim() != '') {
      birthdays = birthdays.filter((item) => {
        return (item.name.toLowerCase().indexOf(filter.toLowerCase()) > -1);
      });
    }
    return birthdays;
  }
}
