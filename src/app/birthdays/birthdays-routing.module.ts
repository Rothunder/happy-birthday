//---------------CORE----------------------------------------------------------------------------------------------------------------------------//
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
//---------------COMPONENTS/PAGES----------------------------------------------------------------------------------------------------------------//
import { BirthdayListPage } from './pages/birthday-list/birthday-list.page';
import { AddBirthdayPage } from './pages/add-birthday/add-birthday.page';
import { BirthdayDetailPage } from './pages/birthday-detail/birthday-detail.page';

const routes: Routes = [
  {
    path: '',
    component: BirthdayListPage,
  },
  {
    path: 'birthdays/add-birthday',
    component: AddBirthdayPage,
    canActivate: [AuthGuard]
  },
  {
    path: 'birthdays/birthday-detail/:name',
    component: BirthdayDetailPage,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: '/birthdays',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BirthdaysRoutingModule { }