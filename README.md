**Happy Birthday Scheduler App**

Send Happy Birthday Email Notifications

---

## Getting started with Ionic

To run the mobile app using Ionic follow these steps

1. npm install -g ionic
2. cd appName
3. npm install
4. ionic serve

For the best in browser mobile experience run the app by toggling  **Toggle Device Toolbar** button in deveolper tools and select your prefered mobile device.
---